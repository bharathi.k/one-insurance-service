const express = require('express');
const app = express();
const port = 3000;

const insuranceController = require('./src/routes/insurance.controller');
const loanController = require('./src/routes/loan.controller');

app.use(express.json());

app.use('/v1/insurance', insuranceController);
app.use('/v1/loan', loanController);

app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send('Internal Server Error');
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
